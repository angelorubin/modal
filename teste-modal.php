<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<title>Modal Jquery</title>
		<!-- jQuery -->
		<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/2.1.1/jquery.min.js" type="text/javascript"></script>
		<!-- Plugin Modal jQuery-->
		<script src="modal.js" type="text/javascript"></script>
		<!-- Css Modal -->
		<link href="modal.css" rel="stylesheet">
		<script>
			$(function(){
				$('#modal').modal();
			});
		</script>
	</head>
	<body>
		<div class="container">
			<div class="row">
				<div class="col-sm-6 col-sm-offset-6 col-md-6 col-md-offset-6">
					<button class="btn btn-primary btn-sm" href="#modal" data-modal="modal">Mostrar Modal</button>
				</div>
			</div>
			<div class="row">
				<div class="col-xs-12 col-sm-12 col-md-12">
					<h2>Exemplos</h2>
					<p>
						Para demonstrar o funcioanamento da declaração height: 100% conforme a teoria relatada nessa matéria criei páginas de exemplos:
						As páginas mostram um elemento div com a classe um (div.um) e respectivo conteúdo, ao qual pretendemos estender a altura por toda a altura da viewport.
						A primeira tentativa é simplesmente declarar a regra de estilo: .um { height: 100%; }. Não vai funcionar, pois o elemento-pai do div.um (body) não tem sua altura declarada.
						Ver Exemplo 1 (abre em nova janela).

						Para funcionar precisamos declarar a altura de TODOS os elementos ancestrais de div.um. São eles: body e html.
						Ver Exemplo 2 (abre em nova janela).

						Talvez você seja tentado a declarar: * { height: 100%; } na esperança de normatizar alturas. Ou ainda, declarar height: 100%; para todos os elementos de um trecho do DOM onde irá precisar de altura estendida. Tal procedimento poderá causar a maior confusão no seu layout.

						Se um elemento ao qual se pretende declarar height: 100% contiver margens e/ou paddings verticais na renderização final irá aparecer barra de rolagem vertical, pois o cálculo da porcentagem se faz primeiro e depois são acrescidas as margens e/ou paddings.
						Ver Exemplo 3 (abre em nova janela). Observe a barra de rolagem no navegador.

						Para eliminar as barras de rolagem devemos subtrair o valor total das margens e/ou paddings verticais de 100%. Caso aqueles espaçamentos sejam definidos em porcentagem simplesmente diminuá-os de 100%, caso em outra unidade de medida use a função calc() das CSS (abre em nova janela).
						Ver Exemplo 4 (abre em nova janela). Observe a ausência de barra de rolagem no navegador.

						Atualização: Conforme apontado pelo James Clébio no comentário no. 7 podemos usar a propriedade box-sizing em lugar da função calc().
					</p>
				</div>
			</div>
		</div>
		<!-- Modal -->
		<div class="modal" id="modal">
			<div class="row">
				<div class="col-xs-9 col-sm-9 col-md-9">
					<h4>Título Modal</h4>
				</div>
				<div class="col-xs-3 col-sm-3 col-md-3">
					<span class="close">x</span>
				</div>
			</div>
		</div>
	</body>
</html>
<!-- Modal -->
<div class="modal" id="modal">
	<div class="row">
		<div class="col-xs-9 col-sm-9 col-md-9">
			<h4>Título Modal</h4>
		</div>
		<div class="col-xs-3 col-sm-3 col-md-3">
			<span class="close">x</span>
		</div>
		<div class="row">
			<div class="col-md-12">
				Conteúdo Modal
			</div>
		</div>
	</div>
</div>

# Modal - Plugin jQuery
* * *
Plugin jQuery, utlizado para criação de janelas modais.
## Como Utilizar
* * *
No seu documento, mais especificamente no cabeçalho entre as tags (<header></header>), coloque o seguintes códigos:

<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/2.1.1/jquery.min.js" type="text/javascript"></script>

<script src="modal.js" type="text/javascript"></script>

<script>
	
	$(function(){
		
		$('#minha-modal').modal();
	
	});

</script>


Em qualquer parte do seu documento coloque a base html da janela modal.


<div class="modal" id="minha-modal">

	<div class="row">
		
		<div class="col-xs-9 col-sm-9 col-md-9">
			
			<h4>Título</h4>
		
		</div>
		
		<div class="col-xs-3 col-sm-3 col-md-3">
			
			<span class="close">X</span>
		
		</div>
	
	</div>
	
	<div class="row">
		
		<div class="col-md-12">
			
			Conteudo - Minha Modal
		
		</div>
	
	</div>

</div>

## Conclusão
* * *
Está feito !

Depois disso é só utilizar sua janela modal em qualquer lugar e o melhor de tudo, padronizada para o seu sistema inteiro, podendo ainda ser customizada !
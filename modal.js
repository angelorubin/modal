(function ($){
	$.fn.modal = function(options) {
		// Default settings
		var defaults = {
			'width': '300px',
			'height': '200px',
			'border': '1px solid #CCC',
			'position':'absolute',
			'z-index': 5001,
			'top': '50%',
    		'left': '50%',
			'margin': '-100px 0 0 -150px',
    		'padding': '5px',
    		'background-color':'white'
		};
		var overlay = {
			'opacity' : 0.4,
			'position': 'absolute',
			'top': 0,
			'left': 0,
			'background-color': 'black',
			'width': '100%',
			'z-index': 5000
      	};
		// Merges the defaults with the options chosen on a single object
		var settings = $.extend( {}, defaults, options );
		// Show Modal
		$("a[data-modal=modal], button[data-modal=modal]").on('click', function(event) {
			event.preventDefault();
			var href = $(this).attr('href');
			id = href.substring(1);
			$('div[id='+id+']').css(settings).fadeIn('slow');
			$('.close').css({'cursor': 'pointer'});
			// Creates a overlay above body
			var docHeight = $(document).height();
			$("body").append("<div class='overlay'></div>");
			$(".overlay").height(docHeight).css(overlay);
		});
		// Close Modal
		$(document).on('click', '.close, .overlay', function() {
			$('.modal, .overlay').fadeOut('fast');
		})
	}
})( jQuery );